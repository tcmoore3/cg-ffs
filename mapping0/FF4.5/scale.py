import os
import sys

assert len(sys.argv) == 3, 'give potential and scale value'
pot = sys.argv[1]
scale = sys.argv[2]

os.system("awk '{print $1, $2*%s, $3*%s}' < %s > temp" % (scale, scale, pot))
os.system("mv temp %s" % pot)
