import os

types = {5: 'water', 0: 'ter2', 1: 'tail', 2: 'nhead', 3:'mhead', 4:'shead'}

for i in range(2, 5):
    for j in range(i, 6):
        oldfile = 'pot_full.time33.t%dt%d.txt' % (i, j)
        newfile = '%s-%s.txt' % (types[i], types[j])
        os.system('cp %s %s' % (oldfile, newfile))
