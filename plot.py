import matplotlib as mpl
mpl.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
from msibi.utils.smoothing import savitzky_golay
import glob
import os.path
import string
import pdb

# set default color cycle to black, red, blue, magenta, orange, green
#mpl.rcParams['axes.color_cycle'] = ['k', 'r', 'b', 'm', '#ffa500', 'g']

def make_label(path):
    dirs = string.split(path)
    label = 'M'
    label += string.split(path, '/')[1][-1]
    label += string.split(path, '/')[2]
    label += ' '
    label += os.path.split(path)[1][:-4]
    return label

D_u = 6.0   # Angstrom
E_u = 0.1   # kcal/mol

#files = list()
base_name = './mapping2/FF7.3/'
dirs = []
dirs.append('./mapping2/FF4.4/')
pairs = ['mhead2-water', 'mhead2-mhead2']
#pairs = ['amide-water', 'mhead2-water', 'stick1-water', 'stick2-water',
#         'tail-water', 'mhead2-mhead2']
#files = [''.join((base_name, x, '.txt')) for x in pairs]
files = []
#for tdir in dirs:
files.append(base_name+'head-water.txt')
files.append(base_name+'chead-water.txt')
files.append(base_name+'head-head.txt')
files.append(base_name+'chead-chead.txt')
files.append('pot.head-chead.txt')
print files
"""
while True:
    mappings = glob.glob('./mapping*/')
    for i, mapping in enumerate(mappings):
        print i, mapping[2:-1]
    mapping = raw_input('Which mapping?\n')
    if not mapping:
        break
    ffs = glob.glob(mappings[int(mapping)] + 'FF*/')
    for i, ff in enumerate(ffs):
        print i, ff[11:-1]
    ff = raw_input('Which FF?\n')
    if not ff:
        break
    pairs = glob.glob(ffs[int(ff)] + '*-*.txt')
    for i, pair in enumerate(pairs):
        print i, os.path.split(pair)[1]
    pair = raw_input('Which pair\n')
    if not pair:
        break
    files.append(pairs[int(pair)])
"""

global_min = 0.0
fig, ax = plt.subplots()
for pair in files:
    data = np.loadtxt(pair)
    if pair == base_name+'head-head.txt':
        data[:, 1] = savitzky_golay(data[:, 1], 9, 3)
    if np.amin(data[10:, 1]) < global_min:
        global_min = np.amin(data[10:, 1])
    plt.plot(np.multiply(D_u, data[10:, 0]),
             np.multiply(E_u, data[10:, 1]),
             label=pair[13:-4])
ax.legend(loc='upper right')
#kTline = [[10, 10], [-2, -2.0+(305.0*1.987e-3)]]
#ax.plot(kTline[0], kTline[1], 'k-')
ax.set_ylim(top=0.5, bottom=E_u * global_min * 1.05)
ax.set_xlabel(u'r, \u00C5', size='x-large')
ax.set_ylabel('V(r), kcal/mol', size='x-large')
ax.grid(True)
fig.tight_layout()
fig.savefig('compare.pdf')
