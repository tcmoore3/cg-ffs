import numpy as np
from msibi.utils.smoothing import savitzky_golay
import matplotlib.pyplot as plt

types = {'amide': [36],
         'mhead2': [45],
         'stick1': [31, 32],
         'stick2': [31, 32, 33, 34]}
for t1, ns in types.iteritems():
    pot = np.loadtxt('{t1}-water.txt'.format(**locals()))
    for n in ns:
        y2 = pot[ns[-1]+1, 1]
        y1 = pot[ns[0]-1, 1]
        x2 = pot[ns[-1]+1, 0]
        x1 = pot[ns[0]-1, 0]
        x = pot[n, 0]
        pot[n, 1] = (y2 - y1) / (x2 - x1) * (x - x1) + y1
    pot[:, 2] = -1.0 * np.gradient(pot[:, 1], pot[1, 0] - pot[0, 0])
    np.savetxt('{t1}-water-fixed.txt'.format(**locals()), pot, fmt='%.8f')
