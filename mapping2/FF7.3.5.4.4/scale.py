import numpy as np
import os

scale = 0.7
types = ['amide', 'mhead2', 'stick1', 'stick2']
for t2 in types:
    fname = 'tail-{t2}.txt'.format(**locals())
    pot = np.loadtxt(fname)
    pot[:, 1] *= scale
    pot[:, 2] *= scale
    np.savetxt(fname, pot, fmt='%.8f')
