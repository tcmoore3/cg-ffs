import os
import numpy as np
import pdb

t1s = ['tail', 'ter2']
t2 = 'chead'

scale = 0.6
for t1 in t1s:
    fn = os.path.join('{t1}-{t2}.txt'.format(**locals()))
    pot = np.loadtxt(fn)
    pot[:, 1] *= scale
    pot[:, 2] *= scale
    np.savetxt(fn, pot, fmt='%.8f')

"""
scale_factor = float(sys.argv[1])
import pdb; pdb.set_trace()
for t1 in t1s:
    fn = '{t1}-{t2}.txt'.format(**locals())
    pot = np.loadtxt(fn)
    pot[:, 1] *= scale_factor
    pot[:, 2] *= scale_factor
    np.savetxt(fn, pot, fmt='%.8f')
"""
