import numpy as np
import os

scale = 1.25
types = ['amide', 'mhead2', 'stick1', 'stick2']
for t2 in types:
    fname = '{t2}-chead.txt'.format(**locals())
    pot = np.loadtxt(fname)
    pot[:, 1] *= scale
    pot[:, 2] *= scale
    np.savetxt(fname, pot, fmt='%.8f')
