import copy
import os

import numpy as np


mhead_init = 0.9  # kJ/mol
tail_init = 0.5  # kJ/mol
kT = 1.987e-3 * 305   # kJ/mol
mhead_init /= kT
tail_init /= kT
mhead2 = np.loadtxt('FF4.4/mhead2-water.txt')
#print mhead_init
#print tail_init
diff_init =  mhead_init - tail_init  # 0.66
diffs = [1, 1.25, 1.5, 1.75, 2.0, 2.25, 2.5, 2.75]
xs = range(7, 7+len(diffs))
for diff, x in zip(diffs, xs):
    new_depth = tail_init + diff
    print new_depth
    # have original mhead_depth and new depth
    # now need to scale mhead to the correct depth
    new_pot = copy.deepcopy(mhead2)
    new_pot[:, 1] *= new_depth / mhead_init
    new_pot[:, 2] *= new_depth / mhead_init
    os.system('cp -r FF4.4 FF4.4.%d' % x)
    filename = 'FF4.4.%d/mhead2-water.txt' % x
    np.savetxt(filename, new_pot, fmt='%.8f')
