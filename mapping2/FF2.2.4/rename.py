import os

types = {5: 'water', 0: 'tail', 1: 'amide', 2: 'mhead2', 3:'stick1', 4:'stick2'}
for i in range(1, 6):
    for j in range(i, 6):
        oldname = 'pot_full.time30.t%dt%d.txt' % (i, j)
        newname = '%s-%s.txt' % (types[i], types[j])
        os.system('cp %s %s' % (oldname, newname))
