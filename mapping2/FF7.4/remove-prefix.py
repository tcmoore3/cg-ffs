import glob
import os


files = glob.glob('step*')
for pfile in files:
    new_name = '.'.join(pfile.split('.')[2:])
    os.system('cp {pfile} {new_name}'.format(**locals()))
