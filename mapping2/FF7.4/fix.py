import numpy as np
from msibi.utils.smoothing import savitzky_golay
import matplotlib.pyplot as plt

types = {'chead': [32],
         'ctail': [40, 43],
         'cterm': [41],
         'chme': [30, 32]}
#'ctail': 51, 'ring': 43, 'cterm': 44, 'chme': 37}
for t1, ns in types.iteritems():
    pot = np.loadtxt('{t1}-water.txt'.format(**locals()))
    for n in ns:
        print pot[n, 1]
        pot[n, 1] = 0.5 * (pot[n-1, 1] + pot[n+1, 1])
        print pot[n, 1]
    pot[:, 2] = -1.0 * np.gradient(pot[:, 1], pot[1, 0] - pot[0, 0])
    np.savetxt('{t1}-water-fixed.txt'.format(**locals()), pot, fmt='%.8f')
