import os
import numpy as np
import pdb

t1s = ['amide', 'mhead2', 'stick1', 'stick2']
t2s = ['ring', 'ctail', 'cterm', 'chme']

scale = 0.25
for t1 in t1s:
    for t2 in t2s: 
        fn = os.path.join('{t1}-{t2}.txt'.format(**locals()))
        pot = np.loadtxt(fn)
        pot[:, 1] *= scale
        pot[:, 2] *= scale
        np.savetxt(fn, pot, fmt='%.8f')

"""
scale_factor = float(sys.argv[1])
import pdb; pdb.set_trace()
for t1 in t1s:
    fn = '{t1}-{t2}.txt'.format(**locals())
    pot = np.loadtxt(fn)
    pot[:, 1] *= scale_factor
    pot[:, 2] *= scale_factor
    np.savetxt(fn, pot, fmt='%.8f')
"""
