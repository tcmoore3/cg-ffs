Forcefields for CG skin lipids.

- mapping0 is the orginal CER mapping
- mapping2 is the new CER, with the CER backbone and sticky spots for H-bonding

# Mapping0
## v0 
The original FF, derived with MS IBI. Notes:

- water-water comes from 'optw9'
- ter2-ter2, tail-tail, and ter2-tail come from ffa 'opt33'
- ter2-water and tail-water from from ffa 'optw18'
- head-tail and head-head interactions come from cer2 opt2'
- head-water comes from cer2 optw2

# Mapping1
## v0

- water-water comes from optw9
